import random

t = [
    "xxxxxxxxx",
    "x------xx",
    "x------xx",
    "x-------x",
    "x-------x",
    "x-------x",
    "x-------x",
    "x---xxxxx",
    "xxxxxxxxx",
]


class Piece:
    def __init__(self, symbole, rotations):
        self.symbole = symbole
        self.rotations = rotations


listePieces = [
    Piece(
        ".",
        [
            [(0, 0), (0, 1), (1, 1), (2, 1), (2, 2)],
            [(0, 0), (1, 0), (1, 1), (1, 2), (2, 2)],
            [(0, 0), (0, 1), (1, 0), (2, 0), (2, -1)],
            [(0, 0), (1, 0), (1, -1), (1, 2), (2, -1)],
        ],
    ),
    Piece(
        "^",
        [
            [(0, 0), (1, 0), (2, 0), (0, 1), (0, 2)],
            [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],
            [(0, 0), (1, 0), (2, 0), (2, -1), (2, -2)],
            [(0, 0), (0, 1), (0, 2), (1, 2), (2, 2)],
        ],
    ),
    Piece(
        "/",
        [
            [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)],
            [(0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)],
        ],
    ),
    Piece(
        "&",
        [
            [(0, 0), (0, 1), (1, 1), (0, 2), (1, 2)],
            [(0, 0), (0, 1), (1, 0), (1, 1), (0, 2)],
            [(0, 0), (1, 0), (1, 1), (2, 0), (2, 1)],
            [(0, 0), (1, 0), (1, -1), (2, 0), (2, -1)],
        ],
    ),
    Piece(
        "$",
        [
            [(0, 0), (0, 1), (1, 0), (0, 2), (1, 2)],
            [(0, 0), (1, 0), (1, 1), (1, 2), (0, 2)],
            [(0, 0), (0, 1), (1, 0), (2, 0), (2, 1)],
            [(0, 0), (0, 1), (1, 1), (2, 1), (2, 0)],
        ],
    ),
    Piece(
        "°",
        [
            [(0, 0), (0, 1), (0, 2), (1, 2), (1, 3)],
            [(0, 0), (1, 0), (2, 0), (2, -1), (3, -1)],
            [(0, 0), (1, 0), (2, 0), (2, 1), (3, 1)],
            [(0, 0), (0, 1), (1, 0), (1, -1), (1, -2)],
            [(0, 0), (0, 1), (0, 2), (1, 0), (1, -1)],
            [(0, 0), (0, 1), (1, 1), (1, 2), (1, 3)],
            [(0, 0), (1, 0), (1, 1), (2, 1), (3, 1)],
            [(0, 0), (1, 0), (1, -1), (2, -1), (3, -1)],
        ],
    ),
    Piece(
        "@",
        [
            [(0, 0), (0, 1), (1, 1), (0, 2), (0, 3)],
            [(0, 0), (0, 1), (0, 2), (1, 2), (0, 3)],
            [(0, 0), (1, 0), (1, 1), (2, 0), (3, 0)],
            [(0, 0), (1, 0), (1, -1), (2, 0), (3, 0)],
            [(0, 0), (1, 0), (1, -1), (1, 1), (1, 2)],
            [(0, 0), (1, 0), (1, 0), (1, -1), (1, -2)],
            [(0, 0), (1, 0), (2, 0), (2, 1), (3, 0)],
            [(0, 0), (1, 0), (2, 0), (2, -1), (3, 0)],
        ],
    ),
    Piece(
        ">",
        [
            [(0, 0), (0, 1), (0, 2), (0, 3), (1, 3)],
            [(0, 0), (1, 0), (1, -1), (1, -2), (1, -3)],
            [(0, 0), (1, 0), (2, 0), (3, 0), (3, 1)],
            [(0, 0), (1, 0), (2, 0), (3, 0), (3, -1)],
            [(0, 0), (1, 0), (0, 1), (0, 2), (0, 3)],
            [(0, 0), (1, 0), (1, 1), (1, 2), (1, 3)],
            [(0, 0), (0, 1), (1, 0), (2, 0), (3, 0)],
            [(0, 0), (0, 1), (1, 1), (2, 1), (3, 1)],
        ],
    ),
]


def pp(t):
    print("")
    for l in t:
        print(l)


def get_cell(tableau, x, y):
    if x < 0 or x >= len(tableau):
        return False
    ligne = tableau[x]
    if y < 0 or y >= len(ligne):
        return False
    return ligne[y]


def ca_rentre(casedepart, symbole, rotation, tableau, dummy):
    x, y = casedepart
    for r in rotation:
        yy,xx = r
        xx = xx + x
        yy = yy + y
        cell = get_cell(tableau, xx, yy)
        if not cell or cell != "-":
            if dummy:
                return False
        else:
            if not dummy:
                ligne = list(tableau[xx])
                ligne[yy] = symbole
                tableau[xx] = "".join(ligne)
    return True


i = 0


def metpiece(case, tableau, listepieces, show):
    global i
    i += 1
    if not listepieces:
        if show:
            pp(tableau)
        return True
    x, y = case
    cell = get_cell(tableau, x, y)
    if not cell:
        return False
    if cell != "-":
        if x == len(tableau[0]) - 1:
            y += 1
            x = 0
        else:
            x += 1
        return metpiece((x, y), tableau, listepieces, show)
    else:
        for p in listepieces:
            for r in p.rotations:
                if ca_rentre(case, p.symbole, r, tableau, dummy=True):
                    oldtableau = tableau.copy()
                    ca_rentre(case, p.symbole, r, tableau, dummy=False)
                    newliste = listepieces.copy()
                    newliste.remove(p)
                    if metpiece(case, tableau, newliste, show):
                        return True
                    tableau = oldtableau
        return False


# random.shuffle(listePieces)
def get_solution(tableau, day, month):
    newtab = tableau.copy()
    month = month - 1
    day = day - 1
    lig = month // 6 + 1
    col = month % 6 + 1
    ligne = list(newtab[lig])
    ligne[col] = "0"
    newtab[lig] = "".join(ligne)
    li = day // 7 + 3
    co = day % 7 + 1
    ligne = list(newtab[li])
    ligne[co] = "0"
    newtab[li] = "".join(ligne)
    metpiece((0, 0), newtab, listePieces, show=True)


# pp(t)
def allyear():
    global i
    for month in range(0, 12):
        for day in range(0, 31):
            newtab = t.copy()
            lig = month // 6 + 1
            col = month % 6 + 1
            ligne = list(newtab[lig])
            ligne[col] = "0"
            newtab[lig] = "".join(ligne)
            li = day // 7 + 3
            co = day % 7 + 1
            ligne = list(newtab[li])
            ligne[co] = "0"
            newtab[li] = "".join(ligne)
            i = 0
            if metpiece((0, 0), newtab, listePieces, show=False):
                print(day + 1, month + 1, " found in ----- >", i, " iterations ")
            else:
                print("Nothing found for ", day + 1, month + 1)


# metpiece((0, 0), t, listePieces)

random.shuffle(listePieces)
allyear()
day = input("Day to find (1..31)? ")
month = input("Month (1..12)? ")
if not get_solution(t, int(day), int(month)):
    print("No solution found…")
print(i, " Iterations has been done")
